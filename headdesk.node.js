(function ($) {

/**
 * Update the summary for the module's vertical tab.
 */
Drupal.behaviors.headdesk_node_summary = {
  attach: function (context) {
    // Use the fieldset class to identify the vertical tab element
    $('fieldset#edit-headdesk', context).drupalSetSummary(function (context) {
      var info = [];
      if ($('#edit-headdesk-active-1', context).attr('checked')) {
        info.push(Drupal.t('State: Draft'));
      }
      else if ($('#edit-headdesk-active-0', context).attr('checked')) {
        info.push(Drupal.t('State: Unpublished'));
      }
      else if ($('#edit-headdesk-active-1--2', context).attr('checked')) {
        info.push(Drupal.t('State: Published'));
      }

      if ($('#edit-headdesk-scheduled', context).attr('checked')) {
        var schedule_date = $('#edit-headdesk-time-container-time-datepicker-popup-0', context).val(),
          schedule_time = $('#edit-headdesk-time-container-time-timeEntry-popup-1', context).val();

        if (schedule_date && schedule_time) {
          info.push(Drupal.t('Scheduled: @date at @time', {'@date': schedule_date, '@time': schedule_time}));
        }
      }

      return info.join('<br />');
    });
  }
};

})(jQuery);

