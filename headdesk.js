(function ($) {

  var checkbox, container;

  function headdesk_display_schedule(speed) {
    // Hack to handle changing state when vertical tab is hidden.
    if (speed === undefined) {
      speed = 'slow';
    }

    if (checkbox.attr('checked')) {
      container.show(speed);
    }
    else {
      container.hide(speed);
    }
  };

  function headdesk_set_checkbox_state() {
    checkbox.attr('disabled', $('#edit-headdesk-active-1').attr('checked'));
  }

  $(document).ready(function($) {
    checkbox = $('#edit-headdesk-scheduled');
    container = $('#edit-headdesk-time-container');

    // Set the initial checkbox state.
    headdesk_set_checkbox_state();

    // The other half of the hidden parent hack.
    headdesk_display_schedule(null);
  });

  Drupal.behaviors.headdesk = {
    attach: function (context, settings) {
      $('#edit-headdesk-scheduled', context).change(function() {
        headdesk_display_schedule();
      });

      $('#edit-headdesk-active input[type="radio"]').change(function() {
        headdesk_set_checkbox_state();
      });
    }
  };

})(jQuery);

