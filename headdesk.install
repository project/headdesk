<?php

/**
 * Implements hook_schema().
 */
function headdesk_schema() {
  $schema = array();
  $schema['headdesk_revision'] = array(
    'description' => 'Head Desk revision state tracking.',
    'fields' => array(
      'hdid' => array(
        'description' => 'The primary key.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'entity_type' => array(
        'description' => 'The type of entity being tracked.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      'entity_id' => array(
        'description' => 'The primary identifier for an entity.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'revision_id' => array(
        'description' => 'The revision identifier for an entity.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'target_state' => array(
        'description' => 'The state the entity should change to.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 1,
      ),
      'state_transition' => array(
        'description' => 'The time when the transition should occur.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'processed' => array(
        'description' => 'Indicates the state transition has been processed.',
        'type' => 'int',
        'size' => 'tiny',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('hdid'),
    'indexes' => array(
      'type_id' => array('entity_type', 'entity_id'),
      'entity_revision' => array('entity_type', 'revision_id'),
      'transition_processed' => array('state_transition', 'processed'),
    ),
    'unique keys' => array(
      'main keys'  => array('entity_type', 'entity_id', 'revision_id', 'target_state', 'state_transition'),
    ),
  );

  $schema['headdesk_draft'] = array(
    'description' => 'Head Desk draft revision tracking.',
    'fields' => array(
      'entity_type' => array(
        'description' => 'The type of entity being tracked.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      'entity_id' => array(
        'description' => 'The primary identifier for an entity.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'revision_id' => array(
        'description' => 'The revision identifier for an entity.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('entity_type', 'entity_id'),
  );

  return $schema;
}
