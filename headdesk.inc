<?php

/**
 * @file
 *  Head Desk shared functions.
 */

/**
 * Delete all schedules for an entity.
 */
function headdesk_delete_schedule($type, $id, $revision_id = NULL) {
  $query = db_delete('headdesk_revision')
    ->condition('entity_type', $type)
    ->condition('entity_id', $id);

  if (NULL !== $revision_id) {
    $query->condition('revision_id', $revision_id);
  }

  $query->execute();
}

/**
 * Delete a schedule by id
 */
function headdesk_delete_schedule_by_id($id) {
  $query = db_delete('headdesk_revision')
    ->condition('hdid', $id);
  return $query->execute();
}

/**
 * Loads all schedules for an entity regardless of revision.
 */
function headdesk_load_entity_schedules($entity_type, $id) {
  $schedules = db_select('headdesk_revision', 'r')
    ->fields('r')
    ->condition('entity_id', $id)
    ->condition('entity_type', $entity_type)
    ->execute();

  $items = array();
  foreach ($schedules as $schedule) {
    if (!isset($items[$schedule->revision_id])) {
      $items[$schedule->revision_id] = array();
    }
    $items[$schedule->revision_id][$schedule->hdid] = $schedule;
  }

  return $items;
}

/**
 * Loads all schedules for an entity revision.
 */
function headdesk_load_revision_schedules($entity_type, $revision) {
  $schedules = db_select('headdesk_revision', 'r')
    ->fields('r', array('hdid', 'target_state', 'state_transition', 'processed'))
    ->condition('revision_id', $revision)
    ->condition('entity_type', $entity_type)
    ->orderBy('r.state_transition')
    ->execute();

  $items = array();
  foreach ($schedules as $schedule) {
    $items[$schedule->hdid] = $schedule;
  }

  return $items;
}

/**
 * Delete draft status for entity.
 */
function headdesk_set_draft($entity_type, $id, $revision = NULL) {

  // Always delete first.
  db_delete('headdesk_draft')
    ->condition('entity_type', $entity_type)
    ->condition('entity_id', $id)
    ->execute();

  if (NULL == $revision) {
    // No new revision.
    return;
  }

  db_insert('headdesk_draft')
    ->fields(array(
      'entity_type' => $entity_type,
      'entity_id' => $id,
      'revision_id' => $revision,
    ))->execute();
}

/**
 * Fetches the draft id for an entity.
 */
function headdesk_current_draft_id($entity_type, $id) {
  $drafts = &drupal_static(__FUNCTION__, array());
  $key = "{$entity_type}_{$id}";
  if (!isset($drafts[$key])) {
    $draft = db_select('headdesk_draft', 'd')
      ->fields('d', array('revision_id'))
      ->condition('d.entity_type', $entity_type)
      ->condition('d.entity_id', $id)
      ->execute()
      ->fetchField();

    $drafts[$key] = $draft;
  }
  return $drafts[$key];
}

/**
 * Fix the revision information for an entity.
 *
 * @param string $entity_type
 *   The type of entity being fixed.
 * @param object $entity
 *   The entity being fixed.
 */
function headdesk_fix_revision($entity_type, $entity) {
  $entity_info = entity_get_info($entity_type);
  $entity->{$entity_info['revision config']['new revision']} = FALSE;
  try {
    entity_save($entity_type, $entity);
  }
  catch (Exception $e) {
    watchdog_exception('headdesk fix revision exception', $e);
  }
}

/**
 * Save a headdesk entry.
 */
function headdesk_save($schedule) {
  $pk = array();
  if (isset($schedule['hdid'])) {
    $pk = array('hdid');
  }

  $ret = drupal_write_record('headdesk_revision', $schedule, $pk);
}

/**
 * Process pending state transitions.
 *
 * @param int $timestamp
 *  Process all transitions due before this time.
 */
function headdesk_process_pending($timestamp) {
  $results = db_select('headdesk_revision', 'r')
    ->fields('r')
    ->condition('r.processed', FALSE)
    ->condition('r.state_transition', $timestamp, '<=')
    ->orderBy('r.state_transition')
    ->execute();

  foreach ($results as $result) {
    $state_label = t('Invalid');
    $entity_info = entity_get_info($result->entity_type);
    $entity = entity_revision_load($result->entity_type, $result->revision_id);
    $entity->{$entity_info['revision config']['new revision']} = FALSE;
    $entity->headdesk->in_update = TRUE;

    if ($result->target_state == $entity_info['revision config']['active state value']) {
      $state_label = $entity_info['revision config']['active state label'];

      entity_revision_set_default($result->entity_type, $entity);
      $entity->{$entity_info['entity keys']['active']} = $result->target_state;
    }
    elseif ($result->target_state == $entity_info['revision config']['inactive state value']) {
      $state_label = $entity_info['revision config']['inactive state label'];
      $entity->{$entity_info['entity keys']['active']} = $result->target_state;
    }

    $saved = entity_save($result->entity_type, $entity);

    headdesk_set_draft($result->entity_type, $result->entity_id);
    db_update('headdesk_revision')
      ->fields(array('processed' => TRUE))
      ->condition('hdid', $result->hdid)
      ->execute();

    $log_args = array(
      '@type' => $result->entity_type,
      '@id' => $result->entity_id,
      '@revision' => $result->revision_id,
      '@state' => $state_label,
    );

    watchdog('headdesk', '@type @id state transition. Revision @revision is now @state.', $log_args);
  }
}

/**
 * TODO Decide if this should be a theme function.
 */
function headdesk_format_schedule($schedule, $include_processed = TRUE) {
  $entity_info = &drupal_static(__FUNCTION__);
  if (!isset($entity_info[$schedule->entity_type])) {
    $entity_info[$schedule->entity_type] = entity_get_info($schedule->entity_type);
  }

  // Make the code more readable.
  $revision_config = $entity_info[$schedule->entity_type]['revision config'];

  $label = $revision_config['active state label'];
  if ($schedule->target_state == $revision_config['inactive state value']) {
    $label = $revision_config['inactive state label'];
  }

  $args = array(
    '@state' => $label,
    '@date_time' => $schedule->state_transition ? format_date($schedule->state_transition, 'short') : t('immediately'),
    '@processed' => $schedule->processed ? t('completed') : t('pending'),
  );

  if (!$include_processed) {
    return t("@state: @date_time", $args);
  }

  return t("@state: @date_time (@processed)", $args);
}

/**
 * Generates form for managing schedules.
 */
function headdesk_manage_schedule($form, $form_state, $entity_type, $revision) {
  $entity_info = entity_get_info($entity_type);
  if (empty($entity_info['revision config'])) {
    // We shouldn't get here, but just in case.
    return drupal_access_denied();
  }

  $entity = entity_revision_load($entity_type, $revision);
  if (!$entity) {
    return drupal_not_found();
  }

  $schedules = array();
  if (!empty($entity->headdesk->schedules)) {
    $schedules = $entity->headdesk->schedules;
  }

  $state_select = array(
    '#type' => 'select',
    '#title' => t('status'),
    '#options' => array(
      $entity_info['revision config']['inactive state value'] => $entity_info['revision config']['inactive state label'],
      $entity_info['revision config']['active state value'] => $entity_info['revision config']['active state label'],
    ),
  );

  $form = array(
    '#theme' => 'headdesk_manage_schedule',
    '#tree' => TRUE,
  );

  $form['entity_type'] = array(
    '#type' => 'value',
    '#value' => $entity_type,
  );

  $form['entity_id'] = array(
    '#type' => 'value',
    '#value' => $entity->{$entity_info['entity keys']['id']},
  );

  $form['revision_id'] = array(
    '#type' => 'value',
    '#value' => $revision,
  );

  $processed = array();
  foreach ($schedules as $id => $schedule) {
    if ($schedule->processed) {
      $schedule->entity_type = $entity_type;
      $processed[$id] = headdesk_format_schedule($schedule, FALSE);
    }
    else {
      $state = $state_select;
      $state['#default_value'] = $schedule->target_state;
      $form['hdid'][$id] = array(
        '#type' => 'value',
        '#value' => $id,
      );
      $form['target_state'][$id] = $state;

      $date = new DateObject($schedule->state_transition);
      $form['state_transition'][$id] = array(
        '#type' => 'date_popup',
        '#default_value' => $date->format(DATE_FORMAT_DATETIME),
      );
      $form['delete'][$id] = array(
        '#type' => 'checkbox',
      );
    }
  }

  // Add an empty draft entry, so the user can add a new item.
  $form['hdid'][-1] = array(
    '#type' => 'value',
    '#value' => -1,
  );

  $state = $state_select;
  $state['#default_value'] = NULL;
  $form['hdid'][-1] = array(
    '#type' => 'value',
    '#value' => -1,
  );
  $form['target_state'][-1] = $state;

  $form['state_transition'][-1] = array(
    '#type' => 'date_popup',
    '#default_value' => NULL,
  );
  $form['delete'][-1] = array(
    '#markup' => t('N/A'),
  );

  $form['processed'] = array(
    '#prefix' => '<h2>',
    '#markup' => t('Processed'),
    '#suffix' => '</h2>',
  );

  $form['processed_list'] = array(
    '#markup' => theme('item_list', array('items' => $processed)),
  );

  $form['pending'] = array(
    '#prefix' => '<h2>',
    '#markup' => t('Pending'),
    '#suffix' => '</h2>',
  );

  $form['actions'] = array(
    '#type' => 'container',
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );


  return $form;
}

/**
 * Validates the schedule management form.
 */
function headdesk_manage_schedule_validate($form, &$form_state) {
  $values = $form_state['values'];
  $now = time();
  foreach ($values['hdid'] as $id) {
    if (-1 == $id && empty($values['state_transition'][$id])) {
      // We don't care about the new entry if there is no date.
      continue;
    }

    if (!empty($values['delete'][$id])) {
      // we don't care about records which are about to be deleted.
      continue;
    }

    $element = "state_transition][{$id}";
    $date = new DateObject($values['state_transition'][$id]);
    if ($date->errors) {
      form_set_error($element, t('You must specify a valid date.'));
    }

    if ($now > $date->format(DATE_FORMAT_UNIX)) {
      form_set_error($element, t('The scheduled time must be in the future.'));
    }
  }
}

/**
 * Validates the schedule management form.
 */
function headdesk_manage_schedule_submit($form, &$form_state) {
  $values = $form_state['values'];
  foreach ($values['hdid'] as $id) {
    if (-1 == $id && empty($values['state_transition'][$id])) {
      // We don't care about the new entry if there is no date.
      continue;
    }

    if (!empty($values['delete'][$id])) {
      headdesk_delete_schedule_by_id($id);
      continue;
    }

    $date = new DateObject($values['state_transition'][$id]);
    $schedule = array(
      'entity_type' => $values['entity_type'],
      'entity_id' => $values['entity_id'],
      'revision_id' => $values['revision_id'],
      'target_state' => $values['target_state'][$id],
      'state_transition' => $date->format(DATE_FORMAT_UNIX),
      'processed' => 0,
    );

    if (-1 != $id) {
      $schedule['hdid'] = $id;
    }

    headdesk_save($schedule);
  }

  drupal_set_message(t('Schedules updated'));

  $redirect = '<front>';

  $entity = entity_revision_load($values['entity_type'], $values['revision_id']);
  $uri = entity_uri($values['entity_type'], $entity);
  if ($uri) {
    $redirect = $uri['path'];
  }

  $form_state['redirect'] = $redirect;
}

/**
 * Themes manage schedule form.
 *
 * @param array $form
 *   The form data to theme.
 *
 * @return string
 *   The rendered form.
 */
function theme_headdesk_manage_schedule($variables) {

  $form = $variables['element'];

  $output = '';

  $header = array(t('State Transition'), t('Status'), t('Delete'));
  $rows = array();

  foreach (array_keys($form['hdid']) as $id) {
    if (!is_int($id)) {
      continue;
    }
    $rows[] = array(
      'data' => array(
        drupal_render($form['hdid'][$id]) . drupal_render($form['state_transition'][$id]),
        drupal_render($form['target_state'][$id]),
        drupal_render($form['delete'][$id]),
      ),
    );
  }

  $table = theme('table', array('header' => $header, 'rows' => $rows));
  $actions = drupal_render($form['actions']);
  $output .= drupal_render_children($form);
  $output .= $table;
  $output .= $actions;

  return $output;
}

/**
 * Generates a scheduling form.
 *
 * @return array
 *  The form structure.
 */
function headdesk_schedule_form($entity_type, $entity, $entity_info) {

  $id = NULL;
  if (isset($entity->{$entity_info['entity keys']['id']})
    && $entity->{$entity_info['entity keys']['id']}) {

    $id = $entity->{$entity_info['entity keys']['id']};
  }

  $revision = NULL;
  if (isset($entity->{$entity_info['entity keys']['revision']})
    && $entity->{$entity_info['entity keys']['revision']}) {

    $revision = $entity->{$entity_info['entity keys']['revision']};
  }

  $schedule_id = NULL;
  if (!empty($entity->schedule)) {
    $current_entry = array_pop($entity->schedule);
    if (!$current_entry->processed) {
      $schudle_id = $current_entry['hdid'];
    }
  }

  $active_options = array(
    -1 => t('Draft'),
    $entity_info['revision config']['inactive state value'] => $entity_info['revision config']['inactive state label'],
    $entity_info['revision config']['active state value'] => $entity_info['revision config']['active state label'],
  );

  $form['headdesk'] = array(
    '#type' => 'fieldset',
    '#title' => t('Schedule'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
    '#group' => 'additional_settings',
    '#attached' => array(
      'js' => array(
        'headdesk' => drupal_get_path('module', 'headdesk') . '/headdesk.js',
      ),
    ),
  );


  $form['headdesk']['active'] = array(
    '#type' => 'radios',
    '#title' => t('Status'),
    '#options' => $active_options,
    '#default_value' => -1, // draft
  );

  $form['headdesk']['scheduled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Schedule'),
  );

  $form['headdesk']['time_container'] = array(
    '#type' => 'container',
  );

  $form['headdesk']['time_container']['time'] = array(
    '#type' => 'date_popup',
    '#title' => t('State transition'),
  );

  $form['headdesk']['schedule_id'] = array(
    '#type' => 'value',
    '#value' => $schedule_id,
  );

  $form['headdesk']['new_draft'] = array(
    '#type' => 'value',
    '#value' => !(empty($entity->headdesk->new_draft)),
  );

  // Shove this here so we don't need to look it up on submission.
  $form['headdesk']['entity'] = array(
    'type' => array(
      '#type' => 'value',
      '#value' => $entity_type,
    ),
    'id' => array(
      '#type' => 'value',
      '#value' => $id
    ),
    'revision' => array(
      '#type' => 'value',
      '#value' => $revision,
    ),
  );

  return $form;
}
